--filepath=O:\Upload\Acc\281\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_MKTSG
--fileheadertext=EDI_MKTSG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\GFProducts\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('MKTSG') as TableName,
wca.mktsg.Actflag,
wca.mktsg.AnnounceDate as Created,
wca.mktsg.Acttime as Changed,
wca.scmst.ISIN,
wca.bond.SecID,
wca.mktsg.ExchgCD,
wca.mktsg.MktSegment,
wca.mktsg.MIC,
wca.mktsg.MktsgID
from wca.mktsg
inner join wca.scexh on wca.mktsg.MktsgID = wca.scexh.MktsgID
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
inner join wca.bond on wca.scmst.secid = wca.bond.secid
where
(isin in (select code from client.pfisin where accid=281 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=281 and actflag='U')
and mktsg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))