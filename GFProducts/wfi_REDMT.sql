--filepath=O:\Upload\Acc\281\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_REDMT
--fileheadertext=EDI_REDMT_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\GFProducts\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('REDMT') as TableName,
wca.redmt.Actflag,
wca.redmt.AnnounceDate as Created,
wca.redmt.Acttime as Changed,
wca.redmt.RedmtID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.redmt.RedemptionDate as RedemDate,
wca.redmt.CurenCD as RedemCurrency,
wca.redmt.RedemptionPrice as RedemPrice,
wca.redmt.MandOptFlag,
wca.redmt.PartFinal,
wca.redmt.RedemptionType as RedemType,
wca.redmt.RedemptionAmount as RedemAmount,
wca.redmt.RedemptionPremium as RedemPremium,
wca.redmt.RedemInPercent,
wca.redmt.PriceAsPercent,
wca.redmt.PremiumAsPercent,
wca.redmt.RedmtNotes as Notes
from wca.redmt
inner join wca.bond on wca.redmt.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=281 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=281 and actflag='U')
and redmt.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))