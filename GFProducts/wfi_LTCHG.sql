--filepath=O:\Upload\Acc\281\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_LTCHG
--fileheadertext=EDI_LTCHG_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\GFProducts\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('LTCHG') as TableName,
wca.ltchg.Actflag,
wca.ltchg.AnnounceDate as Created,
wca.ltchg.Acttime as Changed,
wca.ltchg.LtChgID,
wca.scmst.SecID,
wca.scmst.ISIN,
wca.ltchg.ExchgCD,
wca.ltchg.EffectiveDate,
wca.ltchg.OldLot,
wca.ltchg.OldMinTrdQty,
wca.ltchg.NewLot,
wca.ltchg.NewMinTrdgQty
from wca.ltchg
inner join wca.bond on wca.ltchg.secid = wca.bond.secid
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
where
(isin in (select code from client.pfisin where accid=281 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=281 and actflag='U')
and ltchg.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))