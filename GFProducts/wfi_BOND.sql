--filepath=O:\Upload\Acc\281\Feed\
--filenameprefix=
--filename=yyyymmdd
--filenamealt=select date_format(max(wca.tbl_Opslog.Feeddate),'%Y%m%d' ) from wca.tbl_Opslog where seq = 3
--fileextension=.txt
--suffix=_BOND
--fileheadertext=EDI_BOND_
--fileheaderdate=yyyymmdd
--datadateformat=yyyy/mm/dd
--datatimeformat=
--forcetime=n
--filefootertext=EDI_ENDOFFILE
--fieldseparator=	
--outputstyle=
--archive=y
--archivepath=N:\debt\GFProducts\
--fieldheaders=y
--filetidy=n
--fwoffsets=
--incremental=n
--sevent=n
--shownulls=n
--ZEROROWCHK=N

--# 

SELECT 
upper('BOND') as Tablename,
wca.bond.Actflag,
wca.bond.AnnounceDate as Created,
wca.bond.Acttime as Changed,
wca.bond.SecID,
wca.scmst.ISIN,
wca.bond.BondType,
wca.bond.DebtMarket,
wca.bond.CurenCD as DebtCurrency,
case when wca.bond.largeparvalue<>'' and wca.bond.largeparvalue<> 0 then wca.bond.largeparvalue when wca.bond.parvalue<>'' and wca.bond.parvalue<> 0 then wca.bond.parvalue else wca.scmst.parvalue end as NominalValue,
wca.bond.IssueDate,
wca.bond.IssueCurrency,
wca.bond.IssuePrice,
wca.bond.IssueAmount,
wca.bond.IssueAmountDate,
wca.bond.OutstandingAmount,
wca.bond.OutstandingAmountDate,
wca.bond.InterestBasis,
wca.bond.InterestRate,
wca.bond.InterestAccrualConvention,
wca.bond.InterestPaymentFrequency,
wca.bond.IntCommencementDate,
wca.bond.OddFirstCoupon,
wca.bond.FirstCouponDate,
wca.bond.OddLastCoupon,
wca.bond.LastCouponDate,
wca.bond.InterestPayDate1,
wca.bond.InterestPayDate2,
wca.bond.InterestPayDate3,
wca.bond.InterestPayDate4,
wca.bondx.DomesticTaxRate,
wca.bondx.NonResidentTaxRate,
wca.bond.FRNType,
wca.bond.FRNIndexBenchmark,
wca.bond.Markup as FrnMargin,
wca.bond.MinimumInterestRate as FrnMinInterestRate,
wca.bond.MaximumInterestRate as FrnMaxInterestRate,
wca.bond.Rounding,
wca.bondx.Series,
wca.bondx.Class,
wca.bondx.OnTap,
wca.bondx.MaximumTapAmount,
wca.bondx.TapExpiryDate,
wca.bond.Guaranteed,
wca.bond.SecuredBy,
wca.bond.SecurityCharge,
wca.bond.Subordinate,
wca.bond.SeniorJunior,
wca.bond.WarrantAttached,
wca.bond.MaturityStructure,
wca.bond.Perpetual,
wca.bond.MaturityDate,
wca.bond.MaturityExtendible,
wca.bond.Callable,
wca.bond.Puttable,
wca.bondx.Denomination1,
wca.bondx.Denomination2,
wca.bondx.Denomination3,
wca.bondx.Denomination4,
wca.bondx.Denomination5,
wca.bondx.Denomination6,
wca.bondx.Denomination7,
wca.bondx.MinimumDenomination,
wca.bondx.DenominationMultiple,
wca.bond.Strip,
wca.bond.StripInterestNumber, 
wca.bond.Bondsrc,
wca.bond.MaturityBenchmark,
wca.bond.ConventionMethod,
wca.bond.FrnIntAdjFreq as FrnInterestAdjFreq,
wca.bond.IntBusDayConv as InterestBusDayConv,
wca.bond.InterestCurrency,
wca.bond.MatBusDayConv as MaturityBusDayConv,
wca.bond.MaturityCurrency, 
wca.bondx.TaxRules,
wca.bond.VarIntPayDate as VarInterestPaydate,
wca.bond.PriceAsPercent,
wca.bond.PayOutMode,
wca.bond.Cumulative,
case when wca.bond.matprice<>'' then cast(wca.bond.matprice as decimal(18,4))
     when rtrim(wca.bond.largeparvalue)='' then null
     when rtrim(wca.bond.MatPriceAsPercent)='' then null
     else cast(wca.bond.largeparvalue as decimal(18,0)) * cast(wca.bond.MatPriceAsPercent as decimal(18,4))/100 
     end as MatPrice,
wca.bond.MatPriceAsPercent,
wca.bond.SinkingFund,
wca.bondx.GovCity,
wca.bondx.GovState,
wca.bondx.GovCntry,
wca.bondx.GovLawLkup as GovLaw,
wca.bondx.GoverningLaw as GovLawNotes,
wca.bond.Municipal,
wca.bond.PrivatePlacement,
wca.bond.Syndicated,
wca.bond.Tier,
wca.bond.UppLow,
wca.bond.Collateral,
wca.bond.CoverPool,
wca.bond.PikPay,
wca.bond.YieldAtIssue,
wca.bond.CoCoTrigger,
wca.bond.CoCoAct,
wca.bond.NonViability,
wca.bondx.Taxability,
wca.bond.LatestAppliedINTPYAnlCpnRateDate as LatestApplicablePayDate,
wca.bond.FirstAnnouncementDate,
wca.bond.PerformanceBenchmarkISIN,
wca.bond.Notes
from wca.bond
inner join wca.scmst on wca.bond.secid = wca.scmst.secid
left outer join wca.bondx on wca.bond.secid = wca.bondx.secid
where
(isin in (select code from client.pfisin where accid=281 and actflag='I'))
or 
(isin in (select code from client.pfisin where accid=281 and actflag='U')
and bond.acttime > (select max(feeddate) from wca.tbl_opslog where seq = 3))