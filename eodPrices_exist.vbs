'this script is used to check if the EQ files exist Locally, if not then it will fun Amjad script again
'Original run cmd
'java -jar \\192.168.2.163\Ops\AUTO\Tasks\Prices\eod_prices.jar 20151104 /c

checkZip_exist() ' calls the validation function

'-----------------------------------------------------------------------------------------------

Function checkZip_exist()
	
	Dim objFS, objFolder, colFiles, fileValue, truORfalse
	
	Set objFS = CreateObject("Scripting.FileSystemObject")
	
	Set objFolder = objFS.GetFolder("O:\Datafeed\Prices\EOD_Prices\EQ")
	Set colFiles = objFolder.Files
	
	FormattedDate= Right(Year(Now),4) & Right("0" & Month(Now),2) & Right("0" & Day(Now()-1),2)
	truORfals = False
	
	For Each objFile In colFiles
	
		fileValue = Left(objFile.Name, 8)		  		
		
		If fileValue = FormattedDate Then 
			'WScript.Echo fileValue
			truORfalse = True
		End If
	
	Next
	
	If truORfalse = False Then
		'WScript.Echo "arhh"
		triggerEOD_script( FormattedDate ) 'If false then triggers the function which has Amjad's cmd line
	End If	

End Function 

'-----------------------------------------------------------------------------------------------

Function triggerEOD_script( dateVal )
	
	Dim WshShell
	
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	step1=WshShell.Run ("java -jar \\192.168.2.163\Ops\AUTO\Tasks\Prices\eod_prices.jar " & dateVal & " /c", 1, true)
	
End Function 

'-----------------------------------------------------------------------------------------------