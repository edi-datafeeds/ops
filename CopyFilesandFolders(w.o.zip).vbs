'Put the date ranges as a regular expression in the command-line arguments for this script to work 
'for example beteen the 5th of august 2015 to the 7th of november would be written as something along the lines of 
'2015(0(8(0[5-9]|(1|2)[0-9]|3[01])|9(0[1-9]|(1|2)[0-9]|3[1]))|1(0(0[1-7])))
Option Explicit
Dim Fso, objRE, bMatch, objfs, objshell, objfolder, colfiles, objER, objFile, CFI, CFO, a, b, c, count, oFile, CFA
Set Fso = CreateObject("Scripting.FileSystemObject")
Dim ChosenFolder, ChosenFolder2, ChosenFolder3, FolderName

'ChosenFolder = InputBox ("Please enter the directory for the folder you wish to search through")
'ChosenFolder2 = InputBox ("Please enter the directory for the folder you wish the folders to be copied to")
'ChosenFolder = "C:\Users\n.thane-kitson@EDI.LOCAL\Documents\VBS"

'C:\Users\n.thane-kitson@EDI.LOCAL\Documents\VBS

ChosenFolder = "\\192.168.2.163\ether\Mo\po4_hist"



If Fso.FolderExists(ChosenFolder) Then
	MsgBox "The Folder To Be Searched Through Does Exists"
	c = MsgBox ("Do you want a copy and a list of the folders and files?" & vbCrLf & "Press no if you want just the list of files and folders" , vbYesNo)
	If c = vbYes then
		FolderName = InputBox ("Please name the folder for the folders to be copied into")
		ChosenFolder2 = "C:\Users\n.thane-kitson@EDI.LOCAL\Documents\" & FolderName
		Set CFO = CreateObject("Scripting.FileSystemObject")
		If CFO.FolderExists(ChosenFolder2) Then
			a = MsgBox("Folder already exists. Overwrite it?", vbYesNo, "Overwrite folder that the files will be copied to?")
			If a = vbYes Then
				CFO.DeleteFolder ChosenFolder2
				CFO.CreateFolder ChosenFolder2
				Set oFile = Fso.OpenTextFile(ChosenFolder2 & "\FolderNames_and_FilesWithinThem.txt",2,True)
				oFile.Write Now() & vbCrLf & vbCrLf 
				WScript.Echo Now()
				CopyFolderList ChosenFolder
			Else
				b = MsgBox("The files will not be copied." & vbCrLf & "Do you still wish for the list for the search to be made?", vbYesNo)
				If b = vbYes Then
					Set oFile = Fso.OpenTextFile(ChosenFolder2 & "\FolderNames_and_FilesWithinThem.txt",2,True)
					oFile.Write Now() & vbCrLf & vbCrLf 
					WScript.Echo Now()
					ShowFolderList ChosenFolder
				Else
					MsgBox "You have chosen to do nothing."
				End If
			End If
		Else
			ChosenFolder2 = "C:\Users\n.thane-kitson@EDI.LOCAL\Documents\" & FolderName
			Set CFA = CreateObject("Scripting.FileSystemObject")
			CFA.CreateFolder (ChosenFolder2)
			Set oFile = Fso.OpenTextFile(ChosenFolder2 & "\FolderNames_and_FilesWithinThem.txt",2,True)
			oFile.Write Now() & vbCrLf & vbCrLf 
			WScript.Echo Now()
			CopyFolderList ChosenFolder
		End If
	Else
		FolderName = InputBox ("Please name the folder for the list of files and folders to be copied into")
		ChosenFolder2 = "C:\Users\n.thane-kitson@EDI.LOCAL\Documents\" & FolderName
		Set CFO = CreateObject("Scripting.FileSystemObject")
		If CFO.FolderExists(ChosenFolder2) Then
			Set oFile = Fso.OpenTextFile(ChosenFolder2 & "\FolderNames_and_FilesWithinThem.txt",2,True)
			oFile.Write Now() & vbCrLf & vbCrLf 
			WScript.Echo Now()
			ShowFolderList ChosenFolder
		Else
			MsgBox "The Folder For The List To Be Placed Doesn't Exist"
		End If
	End If
Else
	MsgBox "The Folder To Be Searched Through Doesn't Exist"
End If


Sub CopyFolderList(folderspec)

	Dim fs, f, f1, fc, s
	
	 
	
	Set fs = CreateObject("Scripting.FileSystemObject")
	Set f = fs.GetFolder(folderspec)
	Set fc = f.SubFolders
	Set objRE = New RegExp
		objRE.Global     = True
		objRE.IgnoreCase = False
		objRE.Pattern    = "^[A-z]"
		
	For Each f1 in fc
		bMatch = objRE.Test(f1.Name)
		
		
		If bMatch Then
			WScript.Echo "Name of the folder: " & f1.Name 
			oFile.Write "Name of the folder: " & f1.Name & vbCrLf & vbCrLf 

			count = 0 
			
			Set objFS = CreateObject("Scripting.FileSystemObject")
			Set objShell = CreateObject("WScript.Shell")			
			Set objFolder = objFS.GetFolder(f1)
			Set colFiles = objFolder.Files

			Set objER = New RegExp
				objER.Global     = True
				objER.IgnoreCase = False
				objER.Pattern    = WScript.Arguments(0)
				
			ChosenFolder3 = ChosenFolder2 & "\" & f1.Name	
			CFO.CreateFolder ChosenFolder3	

			For Each objFile In colFiles
			
				bMatch = objER.Test(objFile.Name)
				If bMatch Then
					WScript.Echo objFile.Name
					oFile.Write "	" & objFile.Name & vbCrLf
								
					WScript.Echo f1 & "\" & objFile.Name 
					WScript.Echo ChosenFolder3			
								
					Set CFI = CreateObject("Scripting.FileSystemObject")
					CFI.CopyFile f1 & "\" & objFile.Name, ChosenFolder3 & "\"

					count = count + 1
					
				End If
			Next
							
		oFile.Write vbCrLf & "Number of files in this folder:  " & count & vbCrLf & vbCrLf

		End If
	Next
	WScript.Echo Now()
	oFile.Write Now()
	oFile.Close
	MsgBox "FINISHED"
End Sub


Sub ShowFolderList(folderspec)

	Dim fs, f, f1, fc, s
	
	 
	
	Set fs = CreateObject("Scripting.FileSystemObject")
	Set f = fs.GetFolder(folderspec)
	Set fc = f.SubFolders
	Set objRE = New RegExp
		objRE.Global     = True
		objRE.IgnoreCase = False
		objRE.Pattern    = "^[A-z]"
		
	For Each f1 in fc
		bMatch = objRE.Test(f1.Name)
		
		If bMatch Then
			WScript.Echo "Name of the folder: " & f1.Name 
			oFile.Write "Name of the folder: " & f1.Name & vbCrLf & vbCrLf 

			count = 0 
			
			Set objFS = CreateObject("Scripting.FileSystemObject")
			Set objShell = CreateObject("WScript.Shell")			
			Set objFolder = objFS.GetFolder(f1)
			Set colFiles = objFolder.Files

			Set objER = New RegExp
				objER.Global     = True
				objER.IgnoreCase = False
				objER.Pattern    = WScript.Arguments(0)
				
			ChosenFolder3 = ChosenFolder2 & "\" & f1.Name	
			CFO.CreateFolder ChosenFolder3	

			For Each objFile In colFiles
			
				bMatch = objER.Test(objFile.Name)
				If bMatch Then
					WScript.Echo objFile.Name
					oFile.Write "	" & objFile.Name & vbCrLf
								
					WScript.Echo f1 & "\" & objFile.Name 
					WScript.Echo ChosenFolder3			
								
'					Set CFI = CreateObject("Scripting.FileSystemObject")								
'					CFI.CopyFile f1 & "\" & objFile.Name, ChosenFolder3
					
					dim filesys 
					set filesys=CreateObject("Scripting.FileSystemObject") 
				
					filesys.CopyFile ChosenFolder & "\" & objFile.Name, ChosenFolder3 


					count = count + 1
					
				End If
			Next
							
		oFile.Write vbCrLf & "Number of files in this folder:  " & count & vbCrLf & vbCrLf

		End If
	Next
	WScript.Echo Now()
	oFile.Write Now()
	oFile.Close
	MsgBox "FINISHED"
End Sub