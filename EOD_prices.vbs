Option Explicit
On Error Resume Next
'WScript.Arguments(0) is one of the following EQ | FI | IX | AJ
Dim Fso, objRE, bMatch, objfs, objshell, objfolder, colfiles, objER, objFile, CFI, CFO, a, count, oFile
Set Fso = CreateObject("Scripting.FileSystemObject")
Dim FileSource, TempFolderforZip, ZipDestination, FolderForReport, FolderName, FormattedDate
Dim file,folder
Dim extrafolder
extrafolder = WScript.Arguments(0)

'FormattedDate is today's date 
FormattedDate= Right(Year(Now),2) & Right("0" & Month(Now),2) & Right("0" & Day(Now()-1),2)

'incase a static version of a date is needed, the format is YYMMDD    YY = last two numbers of the year
'FormattedDate="151022"

'FileSource is the folder of which the country folders are subfolders of
FileSource = "Y:\FeedArch\P04"

'TempFolderforZip is the temporary folder that the files will be copied to 
'and may or may not be deleted afterwards depending on if the command has been commented out or not
TempFolderforZip = "O:\Datafeed\Prices\EOD_Prices\tmp"

'ZipDestination is the zip folder that will be created and have the extracted files placed in at the end
ZipDestination = "O:\Datafeed\Prices\EOD_Prices\zip\20" & FormattedDate 

'FolderForReport is the folder where the report of folders and files within them that should have been copied should go
FolderForReport = "O:\Datafeed\Prices\EOD_Prices\report\"


If Fso.FolderExists(FileSource) Then
	MsgBox "The Folder To Be Searched Through Does Exists"

		Set CFO = CreateObject("Scripting.FileSystemObject")
		If CFO.FolderExists(TempFolderforZip) Then
			a = MsgBox("Folder already exists. Overwrite it?", vbYesNo, "Overwrite folder that the files will be copied to?")
			If a = vbYes Then

				CFO.DeleteFolder TempFolderforZip
				CFO.CreateFolder TempFolderforZip
				
			 	Set oFile = Fso.OpenTextFile(FolderForReport & FormattedDate & "_" & WScript.Arguments(0) & "_List.txt",2,True)
				oFile.Write Now() & vbCrLf & vbCrLf 
				'WScript.Echo Now()
				
				Select Case extrafolder
					Case "EQ"
						CopyFolderList FileSource
					Case "AJ"
						CopyFolderListAJ FileSource
					Case "FI"
						CopyFolderList FileSource
					Case "IX"
						CopyFolderList FileSource
				End Select	
				
				Call Zip(TempFolderforZip & "\ *.txt", ZipDestination & "_" & WScript.Arguments(0) & ".zip")
				
				Call zipALL()
			
				'WScript.Echo Now()
				oFile.Write Now()
				oFile.Close
				'MsgBox "FINISHED"
				CFO.DeleteFolder TempFolderforZip
				
			Else
				'MsgBox "You have chosen not to overwite the folder therefore nothing will happen."
			End If	
		Else
		
			CFO.CreateFolder TempFolderforZip
			
		 	Set oFile = Fso.OpenTextFile(FolderForReport & FormattedDate & "_" & WScript.Arguments(0) & "_List.txt",2,True)
			oFile.Write Now() & vbCrLf & vbCrLf 
			'WScript.Echo Now()
			
			Select Case extrafolder
				Case "EQ"
					CopyFolderList FileSource
				Case "AJ"
					CopyFolderListAJ FileSource
				Case "FI"
					CopyFolderList FileSource
				Case "IX"
					CopyFolderList FileSource
			End Select

			Call Zip(TempFolderforZip & "\ *.txt", ZipDestination & "_" & WScript.Arguments(0) & ".zip")
			
			Call zipALL()
		
			'WScript.Echo Now()
			oFile.Write Now()
			oFile.Close
			'MsgBox "FINISHED"
			CFO.DeleteFolder TempFolderforZip
		
		End If
Else
	'MsgBox "The Folder To Be Searched Through Doesn't Exist"
	Set oFile = Fso.OpenTextFile(FolderForReport & FormattedDate & "_" & WScript.Arguments(0) & "_Error.txt",2,True)
	oFile.Write Now() & vbCrLf & vbCrLf & "Error:  The folder did not exist therefore the script could not run"
	oFile.Close
End If

'-----------------------------------------------------------------------------------------------------------------------'

Sub CopyFolderListAJ(FileSource)

	Dim fs, f, f1, fc, s
	
	 
	
	Set fs = CreateObject("Scripting.FileSystemObject")
	Set f = fs.GetFolder(FileSource)
	Set fc = f.SubFolders
	Set objRE = New RegExp
		objRE.Global     = True
		objRE.IgnoreCase = False
		'objRE.Pattern    = "^[A][E]$"
		objRE.Pattern    = "^[A-Z]{2}$"

	
	'For each subfolder within the folder  f1 = non constant representative of each subfolder
	For Each f1 in fc
		bMatch = objRE.Test(f1.Name)
		
		If bMatch Then
		
			Set CFI = CreateObject("Scripting.FileSystemObject")
			If CFI.FolderExists(f1 & "\Adjusted") Then
				count = 0
	
				Set objFS = CreateObject("Scripting.FileSystemObject")
				Set objShell = CreateObject("WScript.Shell")			
				'WScript.Echo "Name of the folder: " & f1.Name & "\Adjusted"
				oFile.Write "Name of the folder: " & f1.Name & "\Adjusted" & vbCrLf & vbCrLf 
	
				Set objFolder = objFS.GetFolder(f1 & "\Adjusted")
				Set colFiles = objFolder.Files
	
				Set objER = New RegExp
					objER.Global     = True
					objER.IgnoreCase = False
					objER.Pattern    = FormattedDate & ".{0,3}\.txt$"
					
	
				For Each objFile In colFiles
				
					bMatch = objER.Test(objFile.Name)
					If bMatch Then
					
						file = objFile.Name
						folder = f1 & "\Adjusted\"
													
						'MsgBox "this is name " & folder & " " & file
						'WScript.Echo folder & file
						
						Set CFO = CreateObject("Scripting.FileSystemObject")
						CFO.CopyFile f1 & "\Adjusted\" & objFile.Name, TempFolderforZip & "\"
						
						count = count + 1
						
						oFile.Write file & vbCrLf 
						
						wscript.sleep 1000
						 
					End If
				Next
				
				oFile.Write vbCrLf & "Number of files in this folder:  " & count & vbCrLf & vbCrLf & vbCrLf
			Else			
				'WScript.Echo f1 & "\Adjusted     This folder doesnt exist.  Moving onto the next folder"
				oFile.Write f1 & "\Adjusted     This folder doesnt exist."
			End If			
			
			
		End If
	Next
End Sub

'-----------------------------------------------------------------------------------------------------------------------'

Sub CopyFolderList(FileSource)

	Dim fs, f, f1, fc, s

	Set fs = CreateObject("Scripting.FileSystemObject")
	Set f = fs.GetFolder(FileSource)
	Set fc = f.SubFolders
	Set objRE = New RegExp
		objRE.Global     = True
		objRE.IgnoreCase = False
		'objRE.Pattern    = "^[A][E]$"
		objRE.Pattern    = "^[A-Z]{2}$"
		
		
	Dim sa, filesInzip, zfile, fso, i : i = 1
	Set sa = CreateObject("Shell.Application")
	

	Set sa = CreateObject("Shell.Application")
	Set filesInzip=sa.NameSpace(folder&file).items

	
	'For each subfolder within the folder  f1 = non constant representative of each subfolder
	For Each f1 in fc
		bMatch = objRE.Test(f1.Name)
		
		If bMatch Then

			count = 0

			Set objFS = CreateObject("Scripting.FileSystemObject")
			Set objShell = CreateObject("WScript.Shell")			
			
			'extrafolder = wscript.arguments(0)
			Set CFI = CreateObject("Scripting.FileSystemObject")
			Select Case extrafolder
				Case "EQ"
					'WScript.Echo "Name of the folder: " & f1.Name 
					oFile.Write "Name of the folder: " & f1.Name & vbCrLf & vbCrLf 
					Set objFolder = objFS.GetFolder(f1)
				Case "FI"
					'WScript.Echo "Name of the folder: " & f1.Name & "\Bond"
					oFile.Write "Name of the folder: " & f1.Name & "\Bond" & vbCrLf & vbCrLf
					If CFI.FolderExists(f1 & "\Bond") Then
						Set objFolder = objFS.GetFolder(f1 & "\Bond")
					Else
						'WScript.Echo f1 & "\Bond     This folder doesnt exist.  Moving onto the next folder"
						oFile.Write f1 & "\Bond     This folder doesnt exist."
					End If
				Case "IX"
				'	WScript.Echo "Name of the folder: " & f1.Name & "\Indices"
					oFile.Write "Name of the folder: " & f1.Name & "\Indices" & vbCrLf & vbCrLf			
					If CFI.FolderExists(f1 & "\Indices") Then
						Set objFolder = objFS.GetFolder(f1 & "\Indices")
					Else
						'WScript.Echo f1 & "\Indices     This folder doesnt exist.  Moving onto the next folder"
						oFile.Write f1 & "\Indices     This folder doesnt exist."
					End If
			End Select
			

			
			Set colFiles = objFolder.Files

			Set objER = New RegExp
				objER.Global     = True
				objER.IgnoreCase = False
				objER.Pattern    = FormattedDate & "\.zip$"
				

			For Each objFile In colFiles
			
				bMatch = objER.Test(objFile.Name)
				If bMatch Then
				
				
					Select Case extrafolder
						Case "EQ"
							file = objFile.Name
							folder = f1 & "\"
							'MsgBox "this is name " & folder & " " & file
							'WScript.Echo folder & file
							Call UnzipFiles(folder,file)
							count = count + 1
						Case "FI"
							file = objFile.Name
							folder = f1 & "\Bond\"
							If CFI.FolderExists(f1 & "\Bond") Then
								'MsgBox "this is name " & folder & " " & file
								'WScript.Echo folder & file
								Call UnzipFiles(folder,file)
								count = count + 1
							End If
						Case "IX"
		
							file = objFile.Name
							folder = f1 & "\Indices\"
							If CFI.FolderExists(f1 & "\Indices") Then
								'MsgBox "this is name " & folder & " " & file
								'WScript.Echo folder & file
								Call UnzipFiles(folder,file)
								count = count + 1
							End If
					End Select
					
					

					
				End If
			Next
			
			Select Case extrafolder
				Case "EQ"
					oFile.Write vbCrLf & "Number of files in this folder:  " & count & vbCrLf & vbCrLf & vbCrLf
				
				Case "FI"
					If CFI.FolderExists(f1 & "\Bond") Then
						oFile.Write vbCrLf & "Number of files in this folder:  " & count & vbCrLf & vbCrLf & vbCrLf
					End If
					
				Case "IX"
					If CFI.FolderExists(f1 & "\Indices") Then
						oFile.Write vbCrLf & "Number of files in this folder:  " & count & vbCrLf & vbCrLf & vbCrLf
					End If
			End Select				
			
		End If
	Next

End Sub

'-----------------------------------------------------------------------------------------------------------------------'

Function UnzipFiles(folder, file)
	Dim sa, filesInzip, pathToZipFile, zfile, fsa, fso, i : i = 1
	'MsgBox folder&file
	Set sa = CreateObject("Shell.Application")
	Set filesInzip=sa.NameSpace(folder&file).Items
	
	'for each file in the zip folder
	For Each zfile In filesInzip
		Set fso = CreateObject("Scripting.FileSystemObject")
		
		If Not fso.FileExists(folder & zfile) Then
			sa.NameSpace(folder).CopyHere(zfile), &H100 
			'MsgBox zfile
			
			Set fsa = CreateObject("Scripting.FileSystemObject")
						
			pathToZipFile = ZipDestination & "_" & WScript.Arguments(0) & ".zip"
			
			'MsgBox folder & zfile & ".txt" & vbCrLf & pathToZipFile & "\"
			fsa.MoveFile folder & zfile & ".txt", TempFolderforZip & "\"
			
			oFile.Write zfile & vbCrLf
				
		End If
	Next
End Function

'-----------------------------------------------------------------------------------------------------------------------'

Function Zip(sFile,sArchiveName)

	Dim oFSO, oShell, aScriptFilename, sScriptFilename, sWorkingDirectory, s7zLocation
	Set oFSO = WScript.CreateObject("Scripting.FileSystemObject")
	Set oShell = WScript.CreateObject("Wscript.Shell")
	
	aScriptFilename = Split(Wscript.ScriptFullName, "\")
	sScriptFilename = aScriptFileName(Ubound(aScriptFilename))
	sWorkingDirectory = Replace(Wscript.ScriptFullName, sScriptFilename, "")
	
	If oFSO.FileExists(sWorkingDirectory & "\" & "7z.exe") Then
		s7zLocation = ""
		'WScript.Echo "yes"
	ElseIf oFSO.FileExists("C:\Program Files\7-Zip\7z.exe") Then
		s7zLocation = "C:\Program Files\7-Zip\"
		'WScript.Echo "maybe"
	Else
		Zip = "Error: Couldn't find 7z.exe"
		'WScript.Echo "no"
	End If
	
	oShell.Run """" & s7zLocation & "7z.exe"" a -tzip -y """ & sArchiveName & """ " _
	& sFile, 0, True   
	
	If oFSO.FileExists(sArchiveName) Then
		Zip = 1
	Else
		Zip = "Error: Archive Creation Failed."
	End If
	
End Function

'-----------------------------------------------------------------------------------------------------------------------'

Function zipALL()

    Dim fso, winShell, MyTarget, MySource, file
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set winShell = createObject("shell.application")

	MyTarget = ZipDestination & "_" & WScript.Arguments(0) & ".zip"
    MySource = TempFolderforZip
    
    'Wscript.Echo "Zipping " & MySource & " and renaming it to " & MyTarget
    
    'create a new clean zip archive
    Set file = fso.CreateTextFile(MyTarget, True)
    file.write("PK" & chr(5) & chr(6) & string(18,chr(0)))
    
    
    file.close
    
    winShell.NameSpace(MyTarget).CopyHere winShell.NameSpace(MySource).Items
    
    do until winShell.namespace(MyTarget).items.count = winShell.namespace(MySource).items.count
        wscript.sleep 1000 
    loop
    
    Set winShell = Nothing
    Set fso = Nothing

End Function